package com.example.dancu.shop;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import Common.Common;
import Interface.ItemClickListener;
import Model.Brand;
import Model.Category;
import ViewHolder.BrandViewHolder;
import ViewHolder.MenuViewHolder;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BrandList extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference brandList;

    String brandId="";

    FirebaseRecyclerAdapter<Brand, BrandViewHolder> adapter;

    SwipeRefreshLayout swipeRefreshLayout;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Set Font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().setDefaultFontPath("fonts/shop_font.otf").setFontAttrId(R.attr.fontPath).build());

        setContentView(R.layout.activity_brand_list);

        //Firebase
        database = FirebaseDatabase.getInstance();
        brandList = database.getReference("Brands");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_brand);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_green_dark, android.R.color.holo_orange_dark, android.R.color.holo_blue_dark);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                //Get Intent here
                if (getIntent() != null)
                    brandId = getIntent().getStringExtra("CategoryId");
                if (!brandId.isEmpty() && brandId != null) {
                    if (Common.isConnectedToInternet(getBaseContext()))
                        loadListBrand(brandId);
                    else {
                        Toast.makeText(BrandList.this, "Please check your connection!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }
        });

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                //Get Intent here
                if (getIntent() != null)
                    brandId = getIntent().getStringExtra("CategoryId");
                if (!brandId.isEmpty() && brandId != null) {
                    if (Common.isConnectedToInternet(getBaseContext()))
                        loadListBrand(brandId);
                    else {
                        Toast.makeText(BrandList.this, "Please check your connection!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }
        });
    }


    private void loadListBrand(String  brandId) {

        Query searchByBrand = brandList.orderByChild("categoryId").equalTo(brandId);

        FirebaseRecyclerOptions<Brand> options = new FirebaseRecyclerOptions.Builder<Brand>()
                .setQuery(searchByBrand,Brand.class)
                .build();


        adapter = new FirebaseRecyclerAdapter<Brand, BrandViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull BrandViewHolder viewHolder, int position, @NonNull Brand model) {
                viewHolder.brand_name.setText(model.getName());
                Picasso.with(getBaseContext()).load(model.getImage()).into(viewHolder.brand_image);
                final Brand local = model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //Start new Activity
                        Intent deviceDetail = new Intent(BrandList.this,DeviceList.class);
                        deviceDetail.putExtra("BrandId",adapter.getRef(position).getKey()); //Send Device Id to new activity
                        startActivity(deviceDetail);
                    }
                });
            }

            @Override
            public BrandViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.brand_item,parent,false);
                return new BrandViewHolder(itemView);
            }
        };
        //Set Adapter
        adapter.startListening();
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(adapter!=null)
            adapter.startListening();
    }
}
