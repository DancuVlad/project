package com.example.dancu.shop;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import Common.Common;
import Model.User;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignUpActivity extends AppCompatActivity {

    private EditText userName, userPassword, userEmail,userPhone, userSecureCode;
    private Button regButton;
    private TextView userLogin;
    private FirebaseAuth firebaseAuth;

    public SignUpActivity() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sign_up);
        setupUIViews();

        firebaseAuth =firebaseAuth.getInstance();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_user = database.getReference("User");

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Common.isConnectedToInternet(getBaseContext())) {

                    if (!validateEmail() | !validateUsername() | !validatePassword() | !validatePhone() | !validateSecureCode()) {
                        return;
                    } else {
                        //Upload data to the database
                        String user_email = userEmail.getText().toString().trim();
                        String user_password = userPassword.getText().toString().trim();

                        firebaseAuth.createUserWithEmailAndPassword(user_email, user_password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    sendEmailVerification();
                                }// else
                                  //  Toast.makeText(SignUpActivity.this, "Registration Failed!", Toast.LENGTH_SHORT).show();
                            }
                        });

                        final ProgressDialog mDialog = new ProgressDialog(SignUpActivity.this);
                        mDialog.setMessage("Please waiting...");
                        mDialog.show();

                        table_user.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                //Check if already user
                                if (dataSnapshot.child(userPhone.getText().toString()).exists()) {
                                    mDialog.dismiss();
                                    Toast.makeText(SignUpActivity.this, "Phone already register!", Toast.LENGTH_SHORT).show();
                                } else {
                                    mDialog.dismiss();
                                    User user = new User(userName.getText().toString(), userEmail.getText().toString(), userPassword.getText().toString(),userSecureCode.getText().toString());
                                    table_user.child(userPhone.getText().toString()).setValue(user);
                                    Toast.makeText(SignUpActivity.this, "Sign up successfully!", Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }
                else {
                    Toast.makeText(SignUpActivity.this, "Please check your connection!", Toast.LENGTH_SHORT).show();
                    return;
                }

            }
        });

        userLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUpActivity.this,MainActivity.class));

            }
        });
    }

    private void setupUIViews(){

        userName = (EditText)findViewById(R.id.etUserName);
        userPassword = (EditText)findViewById(R.id.etUserPassword);
        userEmail = (EditText)findViewById(R.id.etUserEmail);
        userPhone = (EditText)findViewById(R.id.etPhone);
        regButton = (Button)findViewById(R.id.btnRegister);
        userLogin = (TextView)findViewById(R.id.tvUserLogin);
        userSecureCode = (MaterialEditText)findViewById(R.id.etSecureCode);
    }

    private boolean validateEmail() {
        String emailInput =  userEmail.getText().toString().trim();

        if (emailInput.isEmpty()) {
            userEmail.setError("Field can't be empty");
            return false;
        } else {
            userEmail.setError(null);
            return true;
        }
    }

    private boolean validateUsername() {
        String usernameInput = userName.getText().toString().trim();

        if (usernameInput.isEmpty()) {
            userName.setError("Field can't be empty");
            return false;
        } else if (usernameInput.length() > 15) {
            userName.setError("Username too long");
            return false;
        } else {
            userName.setError(null);
            return true;
        }
    }

    private boolean validatePassword() {
        String passwordInput = userPassword.getText().toString().trim();

        if (passwordInput.isEmpty()) {
            userPassword.setError("Field can't be empty");
            return false;
        } else if (passwordInput.length() < 6) {
            userPassword.setError("Password too short");
            return false;
        } else {
            userPassword.setError(null);
            return true;
        }
    }

    private boolean validatePhone() {
        String phoneInput = userPhone.getText().toString().trim();

        if (phoneInput.isEmpty()) {
            userPhone.setError("Field can't be empty");
            return false;
        } else {
            userPhone.setError(null);
            return true;
        }
    }

    private boolean validateSecureCode() {
        String secureCodeInput =  userSecureCode.getText().toString().trim();

        if (secureCodeInput.isEmpty()) {
            userSecureCode.setError("Field can't be empty");
            return false;
        } else {
            userSecureCode.setError(null);
            return true;
        }
    }


    private void sendEmailVerification(){
        FirebaseUser firebaseUser = firebaseAuth.getInstance().getCurrentUser();
        if(firebaseUser!=null){
            firebaseUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(SignUpActivity.this,"Successfully Registered, Verification mail!",Toast.LENGTH_SHORT).show();
                        firebaseAuth.signOut();
                        finish();
                        startActivity(new Intent(SignUpActivity.this,MainActivity.class));
                    }else{
                        Toast.makeText(SignUpActivity.this,"Verification mail hasn`t sent!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
