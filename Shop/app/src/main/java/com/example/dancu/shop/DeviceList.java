package com.example.dancu.shop;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import Common.Common;
import Database.Database;
import Interface.ItemClickListener;
import Model.Category;
import Model.Device;
import Model.Favorites;
import Model.Order;
import ViewHolder.DeviceViewHolder;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DeviceList extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference deviceList;

    String deviceId = "";

    FirebaseRecyclerAdapter<Device, DeviceViewHolder> adapter;

    //Search funtionality

    FirebaseRecyclerAdapter<Device, DeviceViewHolder> searchAdapter;
    List<String> suggestList = new ArrayList<>();
    MaterialSearchBar materialSearchBar;

    //Favorites
    Database localDB;

    //Facebook shared
    CallbackManager callbackManager;
    ShareDialog shareDialog;

    SwipeRefreshLayout swipeRefreshLayout;

    //Create Target from Picasso
    Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            //Create Photo from Bitmap
            SharePhoto photo = new SharePhoto.Builder().setBitmap(bitmap).build();
            if (ShareDialog.canShow(SharePhotoContent.class)) {
                SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(photo).build();
                shareDialog.show(content);
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Set Font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().setDefaultFontPath("fonts/shop_font.otf").setFontAttrId(R.attr.fontPath).build());

        setContentView(R.layout.activity_device_list);

        //Init Facebook
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);

        //Firebase
        database = FirebaseDatabase.getInstance();
        deviceList = database.getReference("Devices");

        //Local DB
        localDB = new Database(this);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_green_dark, android.R.color.holo_orange_dark, android.R.color.holo_blue_dark);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Get Intent here
                if (getIntent() != null)
                    deviceId = getIntent().getStringExtra("BrandId");
                if (!deviceId.isEmpty() && deviceId != null) {
                    if (Common.isConnectedToInternet(getBaseContext()))
                        loadListDevice(deviceId);
                    else {
                        Toast.makeText(DeviceList.this, "Please check your connection!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }
        });

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                //Get Intent here
                if (getIntent() != null)
                    deviceId = getIntent().getStringExtra("BrandId");
                if (!deviceId.isEmpty() && deviceId != null) {
                    if (Common.isConnectedToInternet(getBaseContext()))
                        loadListDevice(deviceId);
                    else {
                        Toast.makeText(DeviceList.this, "Please check your connection!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                //Search
                materialSearchBar = (MaterialSearchBar) findViewById(R.id.searchBar);
                materialSearchBar.setHint("Enter your device");
                loadSuggest();//Write function to load Suggest from Firebase
                materialSearchBar.setCardViewElevation(10);
                materialSearchBar.addTextChangeListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        //When user type their text, we will change suggest list
                        List<String> suggest = new ArrayList<String>();
                        for (String search : suggestList) {
                            if (search.toLowerCase().contains(materialSearchBar.getText().toLowerCase()))
                                suggest.add(search);
                        }
                        materialSearchBar.setLastSuggestions(suggest);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                materialSearchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
                    @Override
                    public void onSearchStateChanged(boolean enabled) {
                        //When Search Bar is close
                        //Restore original suggest adapter
                        if (!enabled)
                            recyclerView.setAdapter(adapter);
                    }

                    @Override
                    public void onSearchConfirmed(CharSequence text) {
                        //When search finish
                        //Show result of search adapter
                        startSearch(text);

                    }

                    @Override
                    public void onButtonClicked(int buttonCode) {

                    }
                });
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_device);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (adapter != null)
            adapter.startListening();
    }

    private void startSearch(CharSequence text) {

        //Create Query by name
        Query searchByName = deviceList.orderByChild("name").equalTo(text.toString());
        //Create options with query
        FirebaseRecyclerOptions<Device> deviceOptions = new FirebaseRecyclerOptions.Builder<Device>().setQuery(searchByName, Device.class).build();

        searchAdapter = new FirebaseRecyclerAdapter<Device, DeviceViewHolder>(deviceOptions) {
            @Override
            protected void onBindViewHolder(@NonNull DeviceViewHolder viewHolder, int position, @NonNull Device model) {
                viewHolder.device_name.setText(model.getName());
                Picasso.with(getBaseContext()).load(model.getImage()).into(viewHolder.device_image);
                final Device local = model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //Start new Activity
                        Intent deviceDetail = new Intent(DeviceList.this, DeviceDetail.class);
                        deviceDetail.putExtra("DeviceId", searchAdapter.getRef(position).getKey()); //Send Device Id to new activity
                        startActivity(deviceDetail);
                    }
                });
            }

            @Override
            public DeviceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_item, parent, false);
                return new DeviceViewHolder(itemView);
            }
        };
        searchAdapter.startListening();
        recyclerView.setAdapter(searchAdapter);//Set adapter dor Recycler View is search result

    }


    private void loadSuggest() {
        deviceList.orderByChild("brandId").equalTo(deviceId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Device item = postSnapshot.getValue(Device.class);
                    suggestList.add(item.getName());//Add bane of device to suggest list
                }
                materialSearchBar.setLastSuggestions(suggestList);//Show suggest device
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void loadListDevice(String deviceId) {

        //Create Query by brandId
        Query searchByName = deviceList.orderByChild("brandId").equalTo(deviceId);
        //Create options with query
        FirebaseRecyclerOptions<Device> deviceOptions = new FirebaseRecyclerOptions.Builder<Device>().setQuery(searchByName, Device.class).build();

        adapter = new FirebaseRecyclerAdapter<Device, DeviceViewHolder>(deviceOptions) {
            @Override
            protected void onBindViewHolder(@NonNull final DeviceViewHolder viewHolder, final int position, @NonNull final Device model) {
                viewHolder.device_name.setText(model.getName());
                viewHolder.device_price.setText(String.format("$ %s", model.getPrice().toString()));
                Picasso.with(getBaseContext()).load(model.getImage()).into(viewHolder.device_image);

                //Quick cart
                viewHolder.quick_cart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        boolean isExist = new Database(getBaseContext()).checkDeviceExists(adapter.getRef(position).getKey(), Common.currentUser.getPhone());

                        if (!isExist) {
                            new Database(getBaseContext()).addToCart(new Order(
                                    Common.currentUser.getPhone(),
                                    adapter.getRef(position).getKey(),
                                    model.getName(),
                                    "1",
                                    model.getPrice(),
                                    model.getDiscount(),
                                    model.getImage()
                            ));
                        } else {
                            new Database(getBaseContext()).increaseCart(Common.currentUser.getPhone(), adapter.getRef(position).getKey());
                        }
                        Toast.makeText(DeviceList.this, "Added to Cart", Toast.LENGTH_SHORT).show();
                    }
                });

                //Add Favorites
                if (localDB.isFavorite(adapter.getRef(position).getKey(), Common.currentUser.getPhone()))
                    viewHolder.fav_image.setImageResource(R.drawable.ic_favorite_black_24dp);


                //Click to change status of Favorites
                viewHolder.fav_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Favorites favorites = new Favorites();
                        favorites.setDeviceId(adapter.getRef(position).getKey());
                        favorites.setDeviceName(model.getName());
                        favorites.setDeviceDescription(model.getDescription());
                        favorites.setDeviceDiscount(model.getDiscount());
                        favorites.setDeviceImage(model.getImage());
                        favorites.setDeviceCategoryId(model.getMenuId());
                        favorites.setUserPhone(Common.currentUser.getPhone());
                        favorites.setDevicePrice(model.getPrice());

                        if (!localDB.isFavorite(adapter.getRef(position).getKey(), Common.currentUser.getPhone())) {
                            localDB.addToFavorites(favorites);
                            viewHolder.fav_image.setImageResource(R.drawable.ic_favorite_black_24dp);
                            Toast.makeText(DeviceList.this, "" + model.getName() + " was added to Favorites", Toast.LENGTH_SHORT).show();
                        } else {
                            localDB.removeFromFavorites(adapter.getRef(position).getKey(), Common.currentUser.getPhone());
                            viewHolder.fav_image.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                            Toast.makeText(DeviceList.this, "" + model.getName() + " was removed drom Favorites", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                final Device local = model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //Start new Activity
                        Intent deviceDetail = new Intent(DeviceList.this, DeviceDetail.class);
                        deviceDetail.putExtra("DeviceId", adapter.getRef(position).getKey()); //Send Device Id to new activity
                        startActivity(deviceDetail);
                    }
                });
            }

            @Override
            public DeviceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_item, parent, false);
                return new DeviceViewHolder(itemView);
            }
        };
        //Set Adapter
        adapter.startListening();
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
