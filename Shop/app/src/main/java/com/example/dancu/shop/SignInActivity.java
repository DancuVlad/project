package com.example.dancu.shop;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;


import Common.Common;
import Model.User;
import io.paperdb.Paper;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class SignInActivity extends AppCompatActivity {

    private EditText Phone;
    private EditText Password;
    private Button Login;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;
    com.rey.material.widget.CheckBox ckbRemember;
    private TextView txtForgotPwd;

    FirebaseDatabase database;
    DatabaseReference table_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sign_in);

        Phone = (EditText) findViewById(R.id.etEmail);
        Password = (EditText) findViewById(R.id.etPassword);
        Login = (Button) findViewById(R.id.btnLogin);
        ckbRemember =(com.rey.material.widget.CheckBox) findViewById(R.id.ckbRemember);
        txtForgotPwd = (TextView)findViewById(R.id.txtForgotPwd);

        //Init Paper
        Paper.init(this);

        //Init Firebase
        database = FirebaseDatabase.getInstance();
        table_user = database.getReference("User");

        txtForgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForgotPwdDialog();
            }
        });

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Common.isConnectedToInternet(getBaseContext())) {

                    if (!validatePassword() | !validatePhone()) {
                        return;
                    } else {

                        //Save user & password
                        if(ckbRemember.isChecked())
                        {
                            Paper.book().write(Common.USER_KEY,Phone.getText().toString());
                            Paper.book().write(Common.PWD_KEY,Password.getText().toString());
                        }

                        table_user.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                //Check if user not exist in database
                                if (dataSnapshot.child(Phone.getText().toString()).exists()) {

                                    //Get User information
                                    User user = dataSnapshot.child(Phone.getText().toString()).getValue(User.class);
                                    user.setPhone(Phone.getText().toString()); //set Email
                                    if (user.getPassword().equals(Password.getText().toString())) {
                                        Intent homeIntent = new Intent(SignInActivity.this, Home.class);
                                        Common.currentUser = user;
                                        startActivity(homeIntent);
                                        finish();

                                        table_user.removeEventListener(this);
                                    } else
                                        Toast.makeText(SignInActivity.this, "Wrong Password!", Toast.LENGTH_LONG).show();

                                } else
                                    Toast.makeText(SignInActivity.this, "User not exist", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }
                else {
                    Toast.makeText(SignInActivity.this, "Please check your connection!", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });

    }

    private void showForgotPwdDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Forgot Password");
        builder.setMessage("Enter yor secure code");

        LayoutInflater inflater = this.getLayoutInflater();
        View forgot_view = inflater.inflate(R.layout.forgot_password_layout,null);

        builder.setView(forgot_view);
        builder.setIcon(R.drawable.ic_security_black_24dp);

        final MaterialEditText etPhone = (MaterialEditText)forgot_view.findViewById(R.id.etPhone);
        final MaterialEditText etSecureCode = (MaterialEditText)forgot_view.findViewById(R.id.etSecureCode);

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Check if user available
                table_user.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.child(etPhone.getText().toString()).getValue(User.class);

                        if(user.getSecureCode().equals(etSecureCode.getText().toString()))
                            Toast.makeText(SignInActivity.this,"Your password: " + user.getPassword(),Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(SignInActivity.this,"Wrong secure code!",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    private boolean validatePhone() {
        String phoneInput =  Phone.getText().toString().trim();

        if (phoneInput.isEmpty()) {
            Phone.setError("Field can't be empty");
            return false;
        } else {
            Phone.setError(null);
            return true;
        }
    }

    private boolean validatePassword() {
        String passwordInput = Password.getText().toString().trim();

        if (passwordInput.isEmpty()) {
            Password.setError("Field can't be empty");
            return false;
        } else {
            Password.setError(null);
            return true;
        }
    }

}
