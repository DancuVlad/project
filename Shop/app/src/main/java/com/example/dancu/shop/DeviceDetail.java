package com.example.dancu.shop;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.C;
import com.stepstone.apprating.listener.RatingDialogListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;

import Common.Common;
import Model.Device;
import Model.Order;
import Database.Database;
import Model.Rating;
import info.hoang8f.widget.FButton;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DeviceDetail extends AppCompatActivity implements RatingDialogListener{

    TextView device_name,device_price,device_desctiption;
    ImageView device_image;
    CollapsingToolbarLayout collapsingToolbarLayout;
    FloatingActionButton btnRating;
    CounterFab btnCart;
    ElegantNumberButton numberButton;
    RatingBar ratingBar;

    String deviceId="";

    FirebaseDatabase database;
    DatabaseReference devices;
    DatabaseReference ratings;

    FButton btnShowComment;

    Device currentDevice;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Set Font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().setDefaultFontPath("fonts/shop_font.otf").setFontAttrId(R.attr.fontPath).build());

        setContentView(R.layout.activity_device_detail);
        btnShowComment = (FButton)findViewById(R.id.btnShowComment);
        btnShowComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DeviceDetail.this,ShowComment.class);
                intent.putExtra(Common.INTENT_DEVICE_ID,deviceId);
                startActivity(intent);
            }
        });

        //Firebase
        database = FirebaseDatabase.getInstance();
        devices = database.getReference("Devices");
        ratings = database.getReference("Rating");

        //Init view
        numberButton = (ElegantNumberButton)findViewById(R.id.number_button);
        btnCart = (CounterFab) findViewById(R.id.btnCart);
        btnRating = (FloatingActionButton)findViewById(R.id.btn_rating);
        ratingBar = (RatingBar)findViewById(R.id.ratingBar);

        btnRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRatingDialog();
            }
        });

        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Database(getBaseContext()).addToCart(new Order(
                        Common.currentUser.getPhone(),
                        deviceId,
                        currentDevice.getName(),
                        numberButton.getNumber(),
                        currentDevice.getPrice(),
                        currentDevice.getDiscount(),
                        currentDevice.getImage()
                ));
                Toast.makeText(DeviceDetail.this, "Added to Cart", Toast.LENGTH_SHORT).show();
            }
        });

        btnCart.setCount(new Database(this).getCountCart(Common.currentUser.getPhone()));


        device_desctiption = (TextView)findViewById(R.id.device_description);
        device_name = (TextView)findViewById(R.id.device_name);
        device_price = (TextView)findViewById(R.id.device_price);
        device_image= (ImageView) findViewById(R.id.img_device);

        collapsingToolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.collapsing);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppbar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppbar);

        //Get Device Id from intent
        if(getIntent() != null)
            deviceId = getIntent().getStringExtra("DeviceId");
        if(!deviceId.isEmpty()){
            if(Common.isConnectedToInternet(getBaseContext()))
            {
                getDetailDevice(deviceId);
                getRatingDevice(deviceId);
            }
            else
            {
                Toast.makeText(DeviceDetail.this,"Please check your connection!",Toast.LENGTH_SHORT).show();
                return;
            }
        }
    }

    private void getRatingDevice(String deviceId) {
        Query deviceRating = ratings.orderByChild("deviceId").equalTo(deviceId);
        deviceRating.addValueEventListener(new ValueEventListener() {
            int count=0,sum=0;
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapshot:dataSnapshot.getChildren())
                {
                    Rating item = postSnapshot.getValue(Rating.class);
                    sum+= Integer.parseInt(item.getRateValue());
                    count++;
                }
                if(count != 0 )
                {
                    float average = sum/count;
                    ratingBar.setRating(average);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        btnCart.setCount(new Database(this).getCountCart(Common.currentUser.getPhone()));
    }

    private void showRatingDialog() {
        new AppRatingDialog.Builder().setPositiveButtonText("Submit").setNegativeButtonText("Cancel")
                .setNoteDescriptions(Arrays.asList("Very Bad","Not Good","Quite Ok","Very Good","Excellent"))
                .setDefaultRating(1).setTitle("Rathe this device").setDescription("Please select some stars and give your feedback").setTitleTextColor(R.color.colorPrimary)
                .setDescriptionTextColor(R.color.colorPrimary).setHint("Please write your comment here...").setHintTextColor(android.R.color.white)
                .setCommentTextColor(R.color.colorPrimaryDark).setWindowAnimation(R.style.RatingDialogFadenAnim).create(DeviceDetail.this).show();
    }

    private void getDetailDevice(String deviceId) {
        devices.child(deviceId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                currentDevice = dataSnapshot.getValue(Device.class);

                //Set Image
                Picasso.with(getBaseContext()).load(currentDevice.getImage()).into(device_image);

                collapsingToolbarLayout.setTitle(currentDevice.getName());

                device_price.setText(currentDevice.getPrice());

                device_name.setText(currentDevice.getName());

                device_desctiption.setText(currentDevice.getDescription());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onPositiveButtonClicked(int value, @NotNull String comments) {
        //Get Rating and upload to firebase
        final Rating rating = new Rating(Common.currentUser.getPhone(),Common.currentUser.getName(),deviceId,String.valueOf(value),comments);

        ratings.push().setValue(rating) .addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(DeviceDetail.this, "Thank you for submit rating!",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onNegativeButtonClicked() {

    }
}
