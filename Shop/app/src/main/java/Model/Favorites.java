package Model;

public class Favorites {
    private String DeviceId, DeviceName, DevicePrice, DeviceCategoryId, DeviceImage, DeviceDiscount, DeviceDescription, UserPhone;

    public Favorites() {
    }

    public Favorites(String deviceId, String deviceName, String devicePrice, String deviceCategoryId, String deviceImage, String deviceDiscount, String deviceDescription, String userPhone) {
        DeviceId = deviceId;
        DeviceName = deviceName;
        DevicePrice = devicePrice;
        DeviceCategoryId = deviceCategoryId;
        DeviceImage = deviceImage;
        DeviceDiscount = deviceDiscount;
        DeviceDescription = deviceDescription;
        UserPhone = userPhone;
    }

    public String getDeviceId() {
        return DeviceId;
    }

    public void setDeviceId(String deviceId) {
        DeviceId = deviceId;
    }

    public String getDeviceName() {
        return DeviceName;
    }

    public void setDeviceName(String deviceName) {
        DeviceName = deviceName;
    }

    public String getDevicePrice() {
        return DevicePrice;
    }

    public void setDevicePrice(String devicePrice) {
        DevicePrice = devicePrice;
    }

    public String getDeviceCategoryId() {
        return DeviceCategoryId;
    }

    public void setDeviceCategoryId(String deviceCategoryId) {
        DeviceCategoryId = deviceCategoryId;
    }

    public String getDeviceImage() {
        return DeviceImage;
    }

    public void setDeviceImage(String deviceImage) {
        DeviceImage = deviceImage;
    }

    public String getDeviceDiscount() {
        return DeviceDiscount;
    }

    public void setDeviceDiscount(String deviceDiscount) {
        DeviceDiscount = deviceDiscount;
    }

    public String getDeviceDescription() {
        return DeviceDescription;
    }

    public void setDeviceDescription(String deviceDescription) {
        DeviceDescription = deviceDescription;
    }

    public String getUserPhone() {
        return UserPhone;
    }

    public void setUserPhone(String userPhone) {
        UserPhone = userPhone;
    }
}
