package Model;


public class Rating {

    private String userPhone;
    private String name;
    private String deviceId;
    private String rateValue;
    private String comment;

    public Rating() {
    }

    public Rating(String userPhone, String name, String deviceId, String rateValue, String comment) {
        this.userPhone = userPhone;
        this.name = name;
        this.deviceId = deviceId;
        this.rateValue = rateValue;
        this.comment = comment;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getRateValue() {
        return rateValue;
    }

    public void setRateValue(String rateValue) {
        this.rateValue = rateValue;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
