package Helper;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.net.Uri;
import android.os.Build;

import com.example.dancu.shop.R;

import Model.Notification;


public class NotificationHelper extends ContextWrapper {

    private static final String CHANEL_ID = "Shop";
    private static final String CHANEL_Name = "Shop";

    private NotificationManager manager;

    public NotificationHelper(Context base) {
        super(base);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            createChannel();
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createChannel() {
        NotificationChannel shopChannel = new NotificationChannel(CHANEL_ID,CHANEL_Name,NotificationManager.IMPORTANCE_DEFAULT);
        shopChannel.enableLights(false);
        shopChannel.enableVibration(true);
        shopChannel.setLockscreenVisibility(android.app.Notification.VISIBILITY_PRIVATE);

        getManager().createNotificationChannel(shopChannel);
    }

    public NotificationManager getManager() {
        if(manager == null)
            manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        return manager;
    }

    @TargetApi(Build.VERSION_CODES.O)
    public android.app.Notification.Builder getShopChannelNotification(String title, String body, PendingIntent contentIntent, Uri soundUri)
    {
        return new android.app.Notification.Builder(getApplicationContext(),CHANEL_ID).setContentIntent(contentIntent).setContentTitle(title).setContentText(body).setSmallIcon(R.mipmap.ic_launcher).setSound(soundUri).setAutoCancel(false);
    }
}
