package Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteQueryBuilder;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

import Model.Favorites;
import Model.Order;

public class Database extends SQLiteAssetHelper {
    private static final String DB_NAME="ShopDB.db";
    private static final int DB_VER=1;
    public Database(Context context) {
        super(context, DB_NAME, null, DB_VER);
    }

    public boolean checkDeviceExists(String deviceId, String userPhone)
    {
        boolean flag = false;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;
        String SQLQuery = String.format("SELECT * FROM OrderDetail WHERE UserPhone='%s' AND ProductId='%s'",userPhone,deviceId);
        cursor = db.rawQuery(SQLQuery,null);
        if(cursor.getCount()>0)
            flag = true;
        else
            flag = false;
        cursor.close();
        return  flag;
    }

    //Cart
    public List<Order> getCarts(String userPhone){
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String[] sqlSelect={"UserPhone","ProductId","ProductName","Quantity","Price","Discount","Image"};
        String sqlTable="OrderDetail";

        qb.setTables(sqlTable);
        Cursor c = qb.query(db,sqlSelect,"UserPhone=?",new String[]{userPhone},null,null,null);
        final List<Order> result= new ArrayList<>();
        if(c.moveToFirst()){
            do{
                result.add(new Order(
                        c.getString(c.getColumnIndex("UserPhone")),
                        c.getString(c.getColumnIndex("ProductId")),
                        c.getString(c.getColumnIndex("ProductName")),
                        c.getString(c.getColumnIndex("Quantity")),
                        c.getString(c.getColumnIndex("Price")),
                        c.getString(c.getColumnIndex("Discount")),
                        c.getString(c.getColumnIndex("Image"))));
            }while (c.moveToNext());
        }
        return result;
    }

    public void addToCart(Order order){
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("INSERT OR REPLACE INTO OrderDetail(UserPhone,ProductId,ProductName,Quantity,Price,Discount,Image) VALUES ('%s','%s','%s','%s','%s','%s','%s');",
                                     order.getUserPhone(),order.getProductId(), order.getProductName(), order.getQuantity(), order.getPrice(), order.getDiscount(), order.getImage());

        db.execSQL(query);
    }

    public void cleanCart(String userPhone){
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("DELETE FROM OrderDetail WHERE UserPhone='%s'",userPhone);

        db.execSQL(query);
    }

    public int getCountCart(String userPhone){
        int count=0;
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("SELECT COUNT(*) FROM OrderDetail Where UserPhone='%s'",userPhone);
        Cursor cursor = db.rawQuery(query,null);
        if(cursor.moveToFirst())
        {
            do {
                count = cursor.getInt(0);
            }while(cursor.moveToNext());
        }
        return count;
    }

    public void updateCart(Order order) {
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("UPDATE OrderDetail SET Quantity= '%s' WHERE UserPhone = '%s' AND ProductId = '%s'",order.getQuantity(),order.getUserPhone(),order.getProductId());
        db.execSQL(query);
    }

    public void increaseCart(String userPhone,String deviceId) {
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("UPDATE OrderDetail SET Quantity= Quantity + 1 WHERE UserPhone = '%s' AND ProductId = '%s'",userPhone,deviceId);
        db.execSQL(query);
    }

    //Favorites
    public void addToFavorites(Favorites device)
    {
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("INSERT INTO Favorites(" +
                "DeviceId,DeviceName,DevicePrice,DeviceCategoryId,DeviceImage,DeviceDiscount,DeviceDescription,UserPhone) " +
                "VALUES('%s','%s','%s','%s','%s','%s','%s','%s');",
                device.getDeviceId(),
                device.getDeviceName(),
                device.getDevicePrice(),
                device.getDeviceCategoryId(),
                device.getDeviceImage(),
                device.getDeviceDiscount(),
                device.getDeviceDescription(),
                device.getUserPhone());
        db.execSQL(query);
    }

    public void removeFromFavorites(String deviceId, String userPhone)
    {
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("DELETE FROM Favorites WHERE DeviceId='%s' and UserPhone='%s';",deviceId,userPhone);
        db.execSQL(query);
    }

    public boolean isFavorite(String deviceId, String userPhone)
    {
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("SELECT * FROM Favorites WHERE DeviceId='%s' and UserPhone='%s';",deviceId,userPhone);
        Cursor cursor = db.rawQuery(query,null);
        if(cursor.getCount()<=0)
        {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void removeFromCart(String productId, String phone) {
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("DELETE FROM OrderDetail WHERE UserPhone='%s' and ProductId='%s'",phone,productId);

        db.execSQL(query);
    }

    public List<Favorites> getaAllFavorites(String userPhone){
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String[] sqlSelect={"UserPhone","DeviceId","DeviceName","DevicePrice","DeviceCategoryId","DeviceImage","DeviceDiscount","DeviceDescription"};
        String sqlTable="Favorites";

        qb.setTables(sqlTable);
        Cursor c = qb.query(db,sqlSelect,"UserPhone=?",new String[]{userPhone},null,null,null);
        final List<Favorites> result= new ArrayList<>();
        if(c.moveToFirst()){
            do{
                result.add(new Favorites(
                        c.getString(c.getColumnIndex("DeviceId")),
                        c.getString(c.getColumnIndex("DeviceName")),
                        c.getString(c.getColumnIndex("DevicePrice")),
                        c.getString(c.getColumnIndex("DeviceCategoryId")),
                        c.getString(c.getColumnIndex("DeviceImage")),
                        c.getString(c.getColumnIndex("DeviceDiscount")),
                        c.getString(c.getColumnIndex("DeviceDescription")),
                        c.getString(c.getColumnIndex("UserPhone"))

                ));
            }while (c.moveToNext());
        }
        return result;
    }
}
