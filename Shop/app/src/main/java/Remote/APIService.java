package Remote;

import Model.MyResponse;
import Model.Sender;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService{
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAv4CQtCs:APA91bH9Ix0kTRy_18fTtxrgrwQVhpX2l3npdaZTgIkrK45DzaiJ-x2Vwf4FzEKhFiz9_fDYxew0EPdf4jDHfyMgXNw9e817EL5_WUFUKV_mYnYNJKJNB5yh2GEvfzrzejN_TE5hbpI"

            }
    )
    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}
