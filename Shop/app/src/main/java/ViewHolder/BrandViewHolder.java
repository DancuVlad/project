package ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dancu.shop.R;

import Interface.ItemClickListener;

public class BrandViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView brand_name;
    public ImageView brand_image;

    private ItemClickListener itemClickListener;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public BrandViewHolder(View itemView) {
        super(itemView);

        brand_name = (TextView)itemView.findViewById(R.id.brand_name);
        brand_image = (ImageView)itemView.findViewById(R.id.brand_image);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition(),false);
    }
}

