package ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.dancu.shop.R;

import Interface.ItemClickListener;

public class FavoritesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView device_name, device_price;
    public ImageView device_image, quick_cart;

    private ItemClickListener itemClickListener;

    public RelativeLayout view_background;
    public LinearLayout view_foregroudn;

    public FavoritesViewHolder(View itemView) {
        super(itemView);

        device_name = (TextView)itemView.findViewById(R.id.device_name);
        device_image = (ImageView)itemView.findViewById(R.id.device_image);
        device_price = (TextView)itemView.findViewById(R.id.device_price);
        quick_cart = (ImageView)itemView.findViewById(R.id.btn_quick_cart);

        view_background = (RelativeLayout)itemView.findViewById(R.id.view_background);
        view_foregroudn = (LinearLayout) itemView.findViewById(R.id.view_foreground);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition(),false);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}