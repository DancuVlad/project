package ViewHolder;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.dancu.shop.DeviceDetail;
import com.example.dancu.shop.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import Common.Common;
import Database.Database;
import Interface.ItemClickListener;
import Model.Favorites;
import Model.Order;

public class FavoritesAdapter extends RecyclerView.Adapter<FavoritesViewHolder> {

    private Context context;
    private List<Favorites> favoritesList;

    public FavoritesAdapter(Context context, List<Favorites> favoritesList) {
        this.context = context;
        this.favoritesList = favoritesList;
    }

    @Override
    public FavoritesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.favorites_item,parent,false);
        return new FavoritesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FavoritesViewHolder viewHolder, final int position) {

        viewHolder.device_name.setText(favoritesList.get(position).getDeviceName());
        viewHolder.device_price.setText(String.format("$ %s", favoritesList.get(position).getDevicePrice().toString()));
        Picasso.with(context).load(favoritesList.get(position).getDeviceImage()).into(viewHolder.device_image);

        //Quick cart
        viewHolder.quick_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean isExist = new Database(context).checkDeviceExists(favoritesList.get(position).getDeviceId(), Common.currentUser.getPhone());

                if (!isExist) {
                    new Database(context).addToCart(new Order(
                            Common.currentUser.getPhone(),
                            favoritesList.get(position).getDeviceId(),
                            favoritesList.get(position).getDeviceName(),
                            "1",
                            favoritesList.get(position).getDevicePrice(),
                            favoritesList.get(position).getDeviceDiscount(),
                            favoritesList.get(position).getDeviceImage()
                    ));
                } else {
                    new Database(context).increaseCart(Common.currentUser.getPhone(), favoritesList.get(position).getDeviceId());
                }
                Toast.makeText(context, "Added to Cart", Toast.LENGTH_SHORT).show();
            }
        });


        final Favorites local =  favoritesList.get(position);
        viewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                //Start new Activity
                Intent deviceDetail = new Intent(context, DeviceDetail.class);
                deviceDetail.putExtra("DeviceId",  favoritesList.get(position).getDeviceId()); //Send Device Id to new activity
                context.startActivity(deviceDetail);
            }
        });

    }

    @Override
    public int getItemCount() {
        return favoritesList.size();
    }

    public void removeItem(int position)
    {
        favoritesList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(Favorites item,int position)
    {
        favoritesList.add(position,item);
        notifyItemInserted(position);
    }

    public Favorites getItem(int position)
    {
        return  favoritesList.get(position);
    }
}
