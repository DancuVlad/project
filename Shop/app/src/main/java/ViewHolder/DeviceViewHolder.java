package ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dancu.shop.R;

import Interface.ItemClickListener;

public class DeviceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView device_name, device_price;
    public ImageView device_image,fav_image, quick_cart;

    private ItemClickListener itemClickListener;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public DeviceViewHolder(View itemView) {
        super(itemView);

        device_name = (TextView)itemView.findViewById(R.id.device_name);
        device_image = (ImageView)itemView.findViewById(R.id.device_image);
        fav_image = (ImageView)itemView.findViewById(R.id.fav);
        device_price = (TextView)itemView.findViewById(R.id.device_price);
        quick_cart = (ImageView)itemView.findViewById(R.id.btn_quick_cart);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition(),false);
    }
}
