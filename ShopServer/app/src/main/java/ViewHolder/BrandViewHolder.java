package ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dancu.shopserver.R;

import Common.Common;
import Interface.ItemClickListener;


public class BrandViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener ,View.OnCreateContextMenuListener{

    public TextView brand_name;
    public ImageView brand_image;

    private ItemClickListener itemClickListener;

    public BrandViewHolder(View itemView) {
        super(itemView);

        brand_name = (TextView)itemView.findViewById(R.id.brand_name);
        brand_image = (ImageView)itemView.findViewById(R.id.brand_image);

        itemView.setOnCreateContextMenuListener(this);//Context menu showed
        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition(),false);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle("Select the action");
        menu.add(0,0,getAdapterPosition(),Common.UPDATE);
        menu.add(0,1,getAdapterPosition(),Common.DELETE);
    }

}
