package com.example.dancu.shopserver;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import Model.ShipperInformation;

public class AllShipper extends FragmentActivity implements OnMapReadyCallback,
    GoogleMap.OnMarkerClickListener{

    private GoogleMap mMap;
    private DatabaseReference shippers;
    Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_shipper);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        shippers = FirebaseDatabase.getInstance().getReference("ShippingOrders");
        shippers.push().setValue(marker);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        googleMap.setOnMarkerClickListener(this);
        //setting the size of marker
        int height = 80;
        int width = 80;
        BitmapDrawable bitmapDrawable = (BitmapDrawable)getResources().getDrawable(R.drawable.car);
        Bitmap bitmap = bitmapDrawable.getBitmap();
        final Bitmap smallMarker = Bitmap.createScaledBitmap(bitmap,width,height,false);
        shippers.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot s: dataSnapshot.getChildren()){
                    ShipperInformation shipper = s.getValue(ShipperInformation.class);
                    LatLng location = new LatLng(Double.parseDouble(String.valueOf(shipper.getLat())),Double.parseDouble(String.valueOf(shipper.getLng())));
                    mMap.addMarker(new MarkerOptions().position(location).title(shipper.getName())).setIcon(BitmapDescriptorFactory.fromBitmap(smallMarker));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(10.0f));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }
}
