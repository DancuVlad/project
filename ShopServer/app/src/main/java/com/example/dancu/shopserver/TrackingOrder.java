package com.example.dancu.shopserver;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.dancu.shopserver.R;
import com.google.android.gms.games.request.Requests;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Common.Common;
import Common.DirectionJSONParser;
import Model.Request;
import Remote.IGeoCoordinates;
import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackingOrder extends FragmentActivity implements OnMapReadyCallback{

    private GoogleMap mMap;

    FirebaseDatabase database;
    DatabaseReference requests,shippingOrder;

    IGeoCoordinates mService;

    Marker shippingMarker;

    Polyline polyline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking_order);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        database = FirebaseDatabase.getInstance();
        requests = database.getReference("Requests");
        mService = Common.getGeoCodeService();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        trakingLocation(Common.currentRequest);
    }

    private void trakingLocation(final Request request) {
        if(request.getAddress() != null && !request.getAddress().isEmpty())
        {
            if(polyline != null)
                polyline.remove();
            //Order location
            mService.getGeoCode(String.format("https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=AIzaSyCtsDUI2h34EsnoNA_aRfbFvXEw_WRYYik",request.getAddress()))
                    .enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            try {
                                JSONObject jsonObject = new JSONObject((response.body().toString()));

                                String lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry").getJSONObject("location").get("lat").toString();

                                String lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry").getJSONObject("location").get("lng").toString();

                                final LatLng orderLocation = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));

                                //Add marker order
                                Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.box);
                                bitmap = Common.scaleBitmap(bitmap,80,80);
                                MarkerOptions markerOrder = new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(bitmap))
                                        .title("Order of "+ Common.currentRequest.getPhone()).position(orderLocation);
                                mMap.addMarker(markerOrder);

                                //Set deposit location
                                LatLng depositLocation = new LatLng(46.112221,24.228491);
                                mMap.moveCamera(CameraUpdateFactory.newLatLng(depositLocation));
                                mMap.animateCamera(CameraUpdateFactory.zoomTo(10.0f));
                                Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(),R.drawable.deposit);
                                bitmap2 = Common.scaleBitmap(bitmap2,80,80);
                                MarkerOptions markerDeposit = new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(bitmap2))
                                        .title("Deposit").position(depositLocation);
                                mMap.addMarker(markerDeposit);

                                //draw route
                                mService.getGeoCode(String.format("https://maps.googleapis.com/maps/api/directions/json?origin=%s,%s&destination=%s&key=AIzaSyAvI2kjGGIf9LaMguuWONGr1vCDgBDzB6c",
                                        depositLocation.latitude,depositLocation.longitude,request.getAddress()))
                                        .enqueue(new Callback<String>() {
                                            @Override
                                            public void onResponse(Call<String> call, Response<String> response) {
                                                new ParserTask().execute(response.body().toString());
                                            }

                                            @Override
                                            public void onFailure(Call<String> call, Throwable t) {

                                            }
                                        });

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {

                        }
                    });
        }
    }

    private class ParserTask extends AsyncTask<String,Integer,List<List<HashMap<String,String>>>> {
        AlertDialog mDialog = new SpotsDialog(TrackingOrder.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog.show();
            mDialog.setMessage("Please waiting...");
        }

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... params) {
            JSONObject jObject;
            List<List<HashMap<String,String>>> routes = null;
            try{
                jObject = new JSONObject(params[0]);
                DirectionJSONParser parser = new DirectionJSONParser();
                routes = parser.parse(jObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return  routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
            mDialog.dismiss();
            ArrayList points = null;
            PolylineOptions lineOptions=null;

            for(int i=0;i<lists.size();i++)
            {
                points = new ArrayList();
                lineOptions = new PolylineOptions();
                List<HashMap<String,String>> path = lists.get(i);

                for(int j=0;j<path.size();j++)
                {
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng =Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat,lng);

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(12);
                lineOptions.color(Color.BLUE);
                lineOptions.geodesic(true);
            }
            polyline = mMap.addPolyline(lineOptions);
        }
    }
}