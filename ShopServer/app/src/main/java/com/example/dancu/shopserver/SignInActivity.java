package com.example.dancu.shopserver;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import Common.Common;
import Model.User;

public class SignInActivity extends AppCompatActivity {

    EditText etPhone, etPassword;
    Button btnSignIn;

    FirebaseDatabase db;
    DatabaseReference users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        etPassword = (MaterialEditText)findViewById(R.id.etPassword);
        etPhone = (MaterialEditText)findViewById(R.id.etPhone);
        btnSignIn = (Button)findViewById(R.id.btnLogin);

        //Init Firebase
        db = FirebaseDatabase.getInstance();
        users = db.getReference("User");

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateEmail()  | !validatePassword() ) {
                    return;
                }else {
                    signInUser(etPhone.getText().toString(), etPassword.getText().toString());
                }
            }
        });
    }

    private void signInUser(String phone, String password) {
        final ProgressDialog mDialog = new ProgressDialog(SignInActivity.this);
        mDialog.setMessage("Please waiting...");
        mDialog.show();

        final String localPhone = phone;
        final  String localPassword = password;
        users.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.child(localPhone).exists()){
                    mDialog.dismiss();
                    User user = dataSnapshot.child(localPhone).getValue(User.class);
                    user.setPhone(localPhone);
                    if(Boolean.parseBoolean(user.getIsStaff())) // If ifStaff == true
                    {
                        if(user.getPassword().equals(localPassword)){
                            //Login succesd
                            Intent login = new Intent(SignInActivity.this,Home.class);
                            Common.currentUser = user;
                            startActivity(login);
                            finish();
                        }
                        else {
                            //Login failed
                            Toast.makeText(SignInActivity.this,"Wrong password!",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(SignInActivity.this,"Please login with Staff account!",Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    mDialog.dismiss();
                    Toast.makeText(SignInActivity.this,"User not exist in DataBase!",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    private boolean validateEmail() {
        String phoneInput =  etPhone.getText().toString().trim();

        if (phoneInput.isEmpty()) {
            etPhone.setError("Field can't be empty");
            return false;
        } else {
            etPhone.setError(null);
            return true;
        }
    }

    private boolean validatePassword() {
        String passwordInput = etPassword.getText().toString().trim();

        if (passwordInput.isEmpty()) {
            etPassword.setError("Field can't be empty");
            return false;
        } else {
            etPassword.setError(null);
            return true;
        }
    }
}
