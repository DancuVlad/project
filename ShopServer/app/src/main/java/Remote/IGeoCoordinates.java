package Remote;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface IGeoCoordinates {
    @GET()
    Call<String> getGeoCode(@Url String url);

}
