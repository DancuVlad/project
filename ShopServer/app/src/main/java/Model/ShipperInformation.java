package Model;

public class ShipperInformation {
    private String orderId,shipperPhone,name;
    private Double lat,lng;

    public ShipperInformation() {
    }

    public ShipperInformation(String orderId, String shipperPhone, String name, Double lat, Double lng) {
        this.orderId = orderId;
        this.shipperPhone = shipperPhone;
        this.name = name;
        this.lat = lat;
        this.lng = lng;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getShipperPhone() {
        return shipperPhone;
    }

    public void setShipperPhone(String shipperPhone) {
        this.shipperPhone = shipperPhone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
