package Model;

public class User {

    private String Name;
    private String Email;
    private String Password;
    private String Phone;
    private String IsStaff;

    public User() {
    }

    public User(String name, String email, String password, String phone, String isStaff) {
        Name = name;
        Email = email;
        Password = password;
        Phone = phone;
        IsStaff = isStaff;
    }

    public String getIsStaff() {
        return IsStaff;
    }

    public void setIsStaff(String isStaff) {
        IsStaff = isStaff;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMail() {
        return Email;
    }

    public void setMail(String mail) {
        Email = mail;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
