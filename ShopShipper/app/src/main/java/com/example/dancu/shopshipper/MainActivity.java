package com.example.dancu.shopshipper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.dancu.shopshipper.Common.Common;
import com.example.dancu.shopshipper.Model.Shipper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import info.hoang8f.widget.FButton;

public class MainActivity extends AppCompatActivity {

    FButton btn_sign_in;
    MaterialEditText edt_phone, edt_password;

    FirebaseDatabase database;
    DatabaseReference shippers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_sign_in = (FButton)findViewById(R.id.btnLogin);
        edt_phone = (MaterialEditText)findViewById(R.id.etPhone);
        edt_password = (MaterialEditText)findViewById(R.id.etPassword);

        //Init Firebase
        database = FirebaseDatabase.getInstance();
        shippers = database.getReference(Common.SHIPPER_TABLE);

        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validatePhone()  | !validatePassword() ) {
                    return;
                }else {
                    login(edt_phone.getText().toString(),edt_password.getText().toString());
                }
            }
        });
    }

    private boolean validatePassword() {
        String passwordInput =  edt_password.getText().toString().trim();

        if (passwordInput.isEmpty()) {
            edt_password.setError("Field can't be empty");
            return false;
        } else {
            edt_password.setError(null);
            return true;
        }
    }

    private boolean validatePhone() {
        String phoneInput =  edt_phone.getText().toString().trim();

        if (phoneInput.isEmpty()) {
            edt_phone.setError("Field can't be empty");
            return false;
        } else {
            edt_phone.setError(null);
            return true;
        }
    }

    private void login(String phone, final String password) {
        shippers.child(phone).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    Shipper shipper = dataSnapshot.getValue(Shipper.class);
                    if(shipper.getPassword().equals(password))
                    {
                        //login succeed
                        startActivity(new Intent(MainActivity.this,HomeActivity.class));
                        Common.currentShipper = shipper;
                        finish();

                    }else{
                        Toast.makeText(MainActivity.this,"Password incorrect!",Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(MainActivity.this,"Your shipper`s phone not exist!",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
