package com.example.dancu.shopshipper;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dancu.shopshipper.Common.Common;
import com.example.dancu.shopshipper.Helper.DirectionJSONParser;
import com.example.dancu.shopshipper.Model.Request;
import com.example.dancu.shopshipper.Remote.IGeoCoordinates;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import info.hoang8f.widget.FButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackingOrder extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    FusedLocationProviderClient fusedLocationProviderClient; //folosit pentru a obtine locatia curenta
    LocationCallback locationCallback;
    LocationRequest locationRequest;//folosit pentru a actualiza locatia

    Location mLastLocation; //ultima locatie

    Marker mCurrentMarker;
    Polyline polyline; // linie folosita pentru desenare

    IGeoCoordinates mService;

    FButton btn_call, btn_shipped;
    TextView txt_direction,txt_duration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking_order);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        btn_call = (FButton) findViewById(R.id.btn_call);
        btn_shipped = (FButton) findViewById(R.id.btn_shipped);
        txt_direction = (TextView)findViewById(R.id.txt_distance);
        txt_duration = (TextView)findViewById(R.id.txt_duration);

        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + Common.currentRequest.getPhone()));
                if (ActivityCompat.checkSelfPermission(TrackingOrder.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                }
                startActivity(intent);
            }
        });

        btn_shipped.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shippedOrder();
            }
        });

        mService = Common.getGeoCodeService();

        buildLocationRequest();
        buildLocationCallBack();

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }

    private void shippedOrder() {
        //delete order in table
        //OrderNeedShip && ShippingOrder
        //and update status of order to Shipped

        FirebaseDatabase.getInstance().getReference(Common.ORDER_NEED_SHIP_TABLE)
                .child(Common.currentShipper.getPhone())
                .child(Common.currentKey).removeValue()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //Update status on Request Table
                Map<String,Object> update_status = new HashMap<>();
                update_status.put("status","03");

                FirebaseDatabase.getInstance().getReference("Requests")
                        .child(Common.currentKey).updateChildren(update_status)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                //Delete from ShippingOrder
                                FirebaseDatabase.getInstance().getReference(Common.SHIPPER_INFO_TABLE)
                                        .child(Common.currentKey)
                                        .removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(TrackingOrder.this,"Shipped!",Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                });
                            }
                        });
            }
        });
    }

    private void buildLocationCallBack() {
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                mLastLocation = locationResult.getLastLocation();

                if(mCurrentMarker != null)
                    mCurrentMarker.setPosition(new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude()));//Update Location for Marker
                //Update Location on Firebase
                Common.updateShippingInformation(Common.currentKey,mLastLocation);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude())));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(16.0f));

                drawRoute(new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude()),Common.currentRequest);
            }
        };

    }

    private void drawRoute(final LatLng yourLocation, final Request request) {

        if(polyline != null)
            polyline.remove();
        if(request.getAddress() != null && !request.getAddress().isEmpty())
        {
            mService.getGeoCode(String.format("https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=AIzaSyCtsDUI2h34EsnoNA_aRfbFvXEw_WRYYik",request.getAddress()))
                    .enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try{
                        final JSONObject jsonObject = new JSONObject((response.body().toString()));

                        String lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry").getJSONObject("location").get("lat").toString();

                        String lng = ((JSONArray)jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry").getJSONObject("location").get("lng").toString();

                        LatLng orderLocation = new LatLng(Double.parseDouble(lat),Double.parseDouble(lng));

                        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.box);
                        bitmap = Common.scaleBitmap(bitmap,80,80);

                        MarkerOptions marker = new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(bitmap)).title("Order of " + Common.currentRequest.getPhone()).position(orderLocation);
                        mMap.addMarker(marker);

                        //Draw route
                        mService.getGeoCode(String.format("https://maps.googleapis.com/maps/api/directions/json?origin=%s,%s&destination=%s,%s&key=AIzaSyAvI2kjGGIf9LaMguuWONGr1vCDgBDzB6c",
                                yourLocation.latitude,yourLocation.longitude,orderLocation.latitude,orderLocation.longitude)).enqueue(new Callback<String>() {
                            JSONArray jRoutes = null;
                            JSONArray jLegs = null;
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                try {
                                    //Show distance and duration
                                    JSONObject jsonObject = new JSONObject((response.body().toString()));
                                    jRoutes = jsonObject.getJSONArray("routes");
                                    jLegs = ((JSONObject) jRoutes.get(0)).getJSONArray("legs");
                                    String disntace = "",duration ="";
                                    //set distance
                                    disntace = (String) ((JSONObject) ((JSONObject) jLegs.get(0)).get("distance")).get("text").toString();
                                    //set duration
                                    duration = (String) ((JSONObject) ((JSONObject) jLegs.get(0)).get("duration")).get("text").toString();
                                    //show distance
                                    txt_direction.setText(disntace);
                                    //show duration
                                    txt_duration.setText(duration);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                new ParserTask().execute(response.body().toString());
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {

                }
            });
        }
    }

    private void buildLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(10f);
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(3000);
    }

    @Override
    protected void onStop() {
        if (fusedLocationProviderClient != null)
            fusedLocationProviderClient.removeLocationUpdates(locationCallback);
        super.onStop();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                mLastLocation = location;
                LatLng yourLocation = new LatLng(location.getLatitude(), location.getLongitude());
                mCurrentMarker = mMap.addMarker(new MarkerOptions().position(yourLocation).title("Your location"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(yourLocation));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(16.0f));
            }
        });

    }

    private class ParserTask extends AsyncTask<String,Integer,List<List<HashMap<String,String>>>> {
        AlertDialog mDialog = new SpotsDialog(TrackingOrder.this);

        @Override
        protected void onPreExecute() {
            //dialog care va fi afisat pana la incarcarea traseului
            super.onPreExecute();
            mDialog.show();
            mDialog.setMessage("Please waiting...");
        }

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... params) {
            JSONObject jObject;
            List<List<HashMap<String,String>>> routes = null;
            //extragem din fisirul json prima ruta, deoarece aceasta este cea mai buna
            try{
                jObject = new JSONObject(params[0]);
                DirectionJSONParser parser = new DirectionJSONParser();
                routes = parser.parse(jObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return  routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
            mDialog.dismiss();
            ArrayList points = null;
            PolylineOptions lineOptions=null;
            //dupa extragerea primei rute
            //extragem din interiorul acesteia calea
            for(int i=0;i<lists.size();i++)
            {
                points = new ArrayList();
                lineOptions = new PolylineOptions();
                List<HashMap<String,String>> path = lists.get(i);
                //dupa extragerea caii
                //trebuie extrasa de aici fiecare latitudine si longitudine
                for(int j=0;j<path.size();j++)
                {
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng =Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat,lng);
                    //adaugam in ArrayList point toate punctele care formeaza calea
                    points.add(position);
                }
                //in functie de punctele din ArrayList le unim intre ele printr-o linie albastra
                lineOptions.addAll(points);
                lineOptions.width(12);
                lineOptions.color(Color.BLUE);
                lineOptions.geodesic(true);
            }
            //afisa pe harta linia care va reprezenta traseul din locatia curenta
            //pana in locatia destinatie
            polyline = mMap.addPolyline(lineOptions);
        }
    }
}
